package com.twuc.webApp.domain.oneToMany.withJoinColumn;

import com.twuc.webApp.domain.JpaTestBase;
import org.hibernate.criterion.InExpression;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class ParentEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Test
    void should_save_and_get_parent_and_child_entity() {
        // TODO
        //
        // 请书写如下的测试：
        //
        // Given Parent 对象和 parent 和 Child 对象 child。它们已经分别进行了持久化。
        // When 将 child 添加到 parent 的 children 集合中。并且进行持久化。
        // Then 再次查询 parent 的时候会发现 parent 的 children 集合中存在 child。
        //
        // <--start--
        flushAndClear(em -> {
            ParentEntity parent = new ParentEntity("parent");
            em.persist(parent);
            ChildEntity child = new ChildEntity("child");
            em.persist(child);
        });

        flushAndClear(em -> {
            ParentEntity parent = em.find(ParentEntity.class, 1L);
            ChildEntity children = em.find(ChildEntity.class, 1L);
            parent.setChildren(children);
            em.persist(parent);
        });

        flushAndClear(em -> {
            ParentEntity parent = em.find(ParentEntity.class, 1L);
            assertEquals("child", parent.getChildren().get(0).getName());
        });
        // --end-->
    }

    @Test
    void should_save_parent_and_child_at_once() {
        // TODO
        //
        // 请书写如下的测试：
        //
        // Given Parent 对象和 parent 和 Child 对象 child。它们均未进行持久化。
        // When 将 child 添加到 parent 的 children 集合中。并且直接对 parent 进行持久化时。
        // Then 再次查询 parent 的时候会发现 parent 的 children 集合中存在 child。
        //
        // <--start-
        flushAndClear(em -> {
            ParentEntity parent = new ParentEntity("parent");
            ChildEntity child = new ChildEntity("child");
            parent.setChildren(child);
            em.persist(parent);
        });

        flushAndClear(em -> {
            ParentEntity parent = em.find(ParentEntity.class, 1L);
            assertEquals("child", parent.getChildren().get(0).getName());
        });
        // --end-->
    }

    @Test
    void should_remove_child() {
        // TODO
        //
        // 请书写如下测试：
        //
        // Given 持久化的 parent 和 child。并且 parent 包含 child。
        // When 当使用某种方法从 parent 中移除 child 并进行持久化。
        // Then 再次查询 parent 的时候，parent 的 children 中已经不包含 child，即关联关系取消了。但是
        //   child 记录本身并未被删除。
        //
        // 在写出测试的过程中也需要考虑如何才能够正确的映射 Entity 呢？
        //
        // <--start-
        flushAndClear(em -> {
            ChildEntity child = new ChildEntity("child");
            em.persist(child);
            ParentEntity parent = new ParentEntity("parent");
            parent.setChildren(child);
            em.persist(parent);
        });

        flushAndClear(em -> {
            ParentEntity parentEntity = em.find(ParentEntity.class, 1L);
            parentEntity.deleteChildren(new ChildEntity("child"));
            em.persist(parentEntity);
        });

        flushAndClear(em -> {
            ParentEntity parent = em.find(ParentEntity.class, 1L);
            assertEquals(0,parent.getChildren().size());

            ChildEntity childEntity = em.find(ChildEntity.class, 1L);
            assertNotNull(childEntity);
            assertEquals("child", childEntity.getName());
        });
        // --end->
    }
}
